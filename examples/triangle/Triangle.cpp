#define STB_IMAGE_IMPLEMENTATION
#include <../../thirdparty/stb/stb_image.h>

#include "GraphicsContext.hpp"
#include "Window.hpp"
#include "Logging.hpp"

struct FrameData {
    glm::vec4 posOffset;
    glm::vec4 colorOffset;
};

struct CameraData {
    glm::mat4 view;
    glm::mat4 projection;
    glm::mat4 viewProjection;
};

struct Vertex {
    glm::vec3 position;
    glm::vec2 uv;
    glm::vec3 normal;
};

int main() {
    Log::init();

    auto window = Window::create("Vulkan Renderer", 1600, 900);

    auto graphicsContext = GraphicsContext::create(window);

    auto commandBuffer = graphicsContext->createFrameBasedCommandBuffer();

    auto fence            = graphicsContext->createFrameBasedFence(true);
    auto renderSemaphore  = graphicsContext->createFrameBasedSemaphore();
    auto presentSemaphore = graphicsContext->createFrameBasedSemaphore();

    PipelineCreateInfo pipelineCreateInfo = {};
    pipelineCreateInfo.vertexShaderPath   = "assets/shaders/mesh.vert";
    pipelineCreateInfo.fragmentShaderPath = "assets/shaders/mesh.frag";
    pipelineCreateInfo.viewportWidth      = window->getWidth();
    pipelineCreateInfo.viewportHeight     = window->getHeight();
    pipelineCreateInfo.culling            = false;
    pipelineCreateInfo.depthTesting       = true;
    pipelineCreateInfo.depthWrite         = true;
    auto pipeline                         = graphicsContext->createPipeline(&pipelineCreateInfo);

    auto cameraDescriptorSet = graphicsContext->createDescriptorSet(pipeline, 0);
    graphicsContext->descriptorSetAddBuffer(cameraDescriptorSet, 0, DescriptorType::UNIFORM_BUFFER,
                                            sizeof(CameraData));

    auto objectsDescriptorSet = graphicsContext->createDescriptorSet(pipeline, 1);
    graphicsContext->descriptorSetAddBuffer(objectsDescriptorSet, 0, DescriptorType::STORAGE_BUFFER,
                                            sizeof(glm::mat4) * 10000);

    int width, height, numComponents;
    unsigned char* textureData =
        stbi_load("assets/textures/grass.jpg", &width, &height, &numComponents, 4);
    auto grassTexture = graphicsContext->createTexture(width, height, 4, textureData);
    stbi_image_free(textureData);

    auto materialDescriptorSet = graphicsContext->createDescriptorSet(pipeline, 2);
    graphicsContext->descriptorSetAddImage(materialDescriptorSet, 0, grassTexture);

    std::vector<Vertex> vertices;
    Vertex vert0;
    vert0.position = glm::vec3(1.0f, 1.0f, 0.0f);
    vert0.uv       = glm::vec2(1.0f, 1.0f);
    Vertex vert1;
    vert1.position = glm::vec3(-1.0f, 1.0f, 0.0f);
    vert1.uv       = glm::vec2(0.0f, 1.0f);
    Vertex vert2;
    vert2.position = glm::vec3(0.0f, -1.0f, 0.0f);
    vert2.uv       = glm::vec2(0.5f, 0.0f);
    vertices.push_back(vert0);
    vertices.push_back(vert1);
    vertices.push_back(vert2);

    auto vertexBuffer = graphicsContext->createVertexBuffer(
        vertices.data(), uint32_t(vertices.size() * sizeof(Vertex)));

    while (!window->shouldClose() && !window->keyDown(GLFW_KEY_ESCAPE)) {
        double startTime = glfwGetTime();
        Window::poll();

        graphicsContext->waitOnFence(fence);
        uint32_t frameIndex = graphicsContext->newFrame(presentSemaphore);
        graphicsContext->beginRecording(commandBuffer);
        graphicsContext->beginSwapchainRenderPass(commandBuffer, frameIndex,
                                                  glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

        graphicsContext->bindPipeline(commandBuffer, pipeline);

        glm::vec3 camPos = { 0.f, 0.0f, -2.0f };
        glm::mat4 view   = glm::translate(glm::mat4(1.0f), camPos);
        // camera projection
        glm::mat4 projection = glm::perspective(glm::radians(70.f), 1600.f / 900.f, 0.1f, 200.0f);
        projection[1][1] *= -1;

        // fill a GPU camera data struct
        CameraData camData;
        camData.projection     = projection;
        camData.view           = view;
        camData.viewProjection = projection * view;
        void* memoryLocation   = graphicsContext->mapDescriptorBuffer(cameraDescriptorSet, 0);
        memcpy(memoryLocation, &camData, sizeof(CameraData));
        graphicsContext->unmapDescriptorBuffer(cameraDescriptorSet, 0);

        void* objectMemoryLocation  = graphicsContext->mapDescriptorBuffer(objectsDescriptorSet, 0);
        glm::mat4* objectMemoryMats = (glm::mat4*)objectMemoryLocation;
        objectMemoryMats[0] =
            glm::rotate(glm::mat4(1.0f), (float)glfwGetTime(), glm::vec3(0, 1, 0));
        graphicsContext->unmapDescriptorBuffer(objectsDescriptorSet, 0);

        graphicsContext->bindDescriptorSet(commandBuffer, 0, cameraDescriptorSet);
        graphicsContext->bindDescriptorSet(commandBuffer, 1, objectsDescriptorSet);
        graphicsContext->bindDescriptorSet(commandBuffer, 2, materialDescriptorSet);
        graphicsContext->bindVertexBuffer(commandBuffer, vertexBuffer);
        graphicsContext->draw(commandBuffer, (uint32_t)vertices.size(), 1, 0, 0);

        graphicsContext->endRenderPass(commandBuffer);
        graphicsContext->endRecording(commandBuffer);
        graphicsContext->submit(commandBuffer, presentSemaphore, renderSemaphore, fence);
        graphicsContext->present(frameIndex, renderSemaphore);

        Log::coreInfo("FPS: {0}", 1.0f / (glfwGetTime() - startTime));
    }

    graphicsContext->waitOnFence(fence, -1);

    return 0;
}