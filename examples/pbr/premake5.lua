project "pbr"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    
    targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
    objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")
    debugdir ("../../")

    files {
        "**.hpp",
        "**.cpp"
    }

    includedirs {
        "./",
        "../../VkPBR/src"
    }
    addIncludes("../../VkPBR/")
    
    links {
        "VkPBR"
    }

    links {
        "glfw",
        "imgui",
        "shaderc_combined",
        "spirv-reflect",
        "vk-bootstrap"
    }

    filter "configurations:Debug"
        defines {
            "DEBUG",
            "_DEBUG"
        }
        symbols "On"

    filter "configurations:Release"
        defines "RELEASE"
        optimize "On"

    filter "system:windows"
        libdirs {
            "%{LibDir.vulkan}/"
        }

        links {
            "vulkan-1"
        }

    filter "system:linux"
        links {
            "dl",
            "pthread",
            "vulkan"
        }