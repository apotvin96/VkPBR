group "thirdparty"
    project "glfw"
        kind "StaticLib"
        language "C"

        targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
        objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")

        files {
            "glfw/include/GLFW/glfw3.h",
            "glfw/include/GLFW/glfw3native.h",
            "glfw/src/glfw_config.h",
            "glfw/src/context.c",
            "glfw/src/init.c",
            "glfw/src/input.c",
            "glfw/src/monitor.c",
            "glfw/src/vulkan.c",
            "glfw/src/window.c"
        }
        filter "system:linux"
            pic "On"

            systemversion "latest"
            staticruntime "On"

            files {
                "glfw/src/x11_init.c",
                "glfw/src/x11_monitor.c",
                "glfw/src/x11_window.c",
                "glfw/src/xkb_unicode.c",
                "glfw/src/posix_time.c",
                "glfw/src/posix_thread.c",
                "glfw/src/glx_context.c",
                "glfw/src/egl_context.c",
                "glfw/src/osmesa_context.c",
                "glfw/src/linux_joystick.c"
            }

            defines {
                "_GLFW_X11"
            }

        filter "system:windows"
            systemversion "latest"
            staticruntime "On"

            files {
                "glfw/src/win32_init.c",
                "glfw/src/win32_joystick.c",
                "glfw/src/win32_monitor.c",
                "glfw/src/win32_time.c",
                "glfw/src/win32_thread.c",
                "glfw/src/win32_window.c",
                "glfw/src/wgl_context.c",
                "glfw/src/egl_context.c",
                "glfw/src/osmesa_context.c"
            }

            defines { 
                "_GLFW_WIN32",
                "_CRT_SECURE_NO_WARNINGS"
            }

        filter "configurations:Debug"
            runtime "Debug"
            symbols "on"

        filter "configurations:Release"
            runtime "Release"
            optimize "on"

    project "imgui"
        kind "StaticLib"
        language "c++"

        targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
        objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")

        includedirs {
            "imgui",
            "glfw/include",
            "%{vulkan_path}/include"
        }

        files {
            "imgui/imconfig.h",
			"imgui/imgui.cpp",
			"imgui/imgui.h",
			"imgui/imgui_demo.cpp",
			"imgui/imgui_draw.cpp",
			"imgui/imgui_internal.h",
			"imgui/imgui_tables.cpp",
			"imgui/imgui_widgets.cpp",
			"imgui/imstb_rectpack.h",
			"imgui/imstb_textedit.h",
			"imgui/imstb_truetype.h",
			"imgui/misc/cpp/imgui_stdlib.cpp",
			"imgui/misc/cpp/imgui_stdlib.h",
            "imgui/backends/imgui_impl_vulkan.cpp",
            "imgui/backends/imgui_impl_vulkan.h",
            "imgui/backends/imgui_impl_glfw.cpp",
            "imgui/backends/imgui_impl_glfw.h"
        }

        filter "configurations:Debug"
            runtime "Debug"
            symbols "on"

        filter "configurations:Release"
            runtime "Release"
            optimize "on"

    project "spirv-reflect"
        kind "StaticLib"
        language "c++"
        
        staticruntime "on"
        
        targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
        objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")
        
        includedirs {
            "../%{IncludeDir.vulkan}"
        }

        files {
            "SPIRV-Reflect/spirv_reflect.c",
            "SPIRV-Reflect/spirv_reflect.h"
        }

        filter "configurations:Debug"
            runtime "Debug"
            symbols "on"

        filter "configurations:Release"
            runtime "Release"
            optimize "on"

    project "vk-bootstrap"
        kind "StaticLib"
        language "c++"

        staticruntime "on"

        targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
        objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")

        includedirs {
            "../%{IncludeDir.vulkan}"
        }

        files {
            "vk-bootstrap/src/VkBootstrap.cpp",
            "vk-bootstrap/src/VkBootstrap.h"
        }

        filter "configurations:Debug"
            runtime "Debug"
            symbols "on"

        filter "configurations:Release"
            runtime "Release"
            optimize "on"
group ""