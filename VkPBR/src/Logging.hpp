#pragma once

#include "pch.hpp"

class Log {
public:
    static bool init();

    template<typename... Args> static void coreTrace(Args&&... args) {
        coreLogger->trace(std::forward<Args>(args)...);
    }

    template<typename... Args> static void coreInfo(Args&&... args) {
        coreLogger->info(std::forward<Args>(args)...);
    }

    template<typename... Args> static void coreWarn(Args&&... args) {
        coreLogger->warn(std::forward<Args>(args)...);
    }

    template<typename... Args> static void coreError(Args&&... args) {
        coreLogger->error(std::forward<Args>(args)...);
    }

    template<typename... Args> static void coreCritical(Args&&... args) {
        coreLogger->critical(std::forward<Args>(args)...);
    }

    template<typename... Args> static void clientTrace(Args&&... args) {
        coreLogger->trace(std::forward<Args>(args)...);
    }

    template<typename... Args> static void clientInfo(Args&&... args) {
        coreLogger->info(std::forward<Args>(args)...);
    }

    template<typename... Args> static void clientWarn(Args&&... args) {
        coreLogger->warn(std::forward<Args>(args)...);
    }

    template<typename... Args> static void clientError(Args&&... args) {
        coreLogger->error(std::forward<Args>(args)...);
    }

    template<typename... Args> static void clientCritical(Args&&... args) {
        coreLogger->critical(std::forward<Args>(args)...);
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugUtilsMessengerCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* callbackData, void* userData);

protected:
private:
    static std::unique_ptr<spdlog::logger> coreLogger;
    static std::unique_ptr<spdlog::logger> clientLogger;
};