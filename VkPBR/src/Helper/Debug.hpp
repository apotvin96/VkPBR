#pragma once

#define VK_CHECK(x)                                                                                \
    do {                                                                                           \
        VkResult err = x;                                                                          \
        if (err) {                                                                                 \
            Log::coreError("Detected Vulkan error: {0}", err);                                     \
            abort();                                                                               \
        }                                                                                          \
    } while (0)
