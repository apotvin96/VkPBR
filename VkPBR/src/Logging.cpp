#include "pch.hpp"
#include "Logging.hpp"

std::unique_ptr<spdlog::logger> Log::coreLogger;
std::unique_ptr<spdlog::logger> Log::clientLogger;

bool Log::init() {
    std::vector<spdlog::sink_ptr> logSinks;
    logSinks.emplace_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    logSinks.emplace_back(
        std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/VkPBR.log", true));

    logSinks[0]->set_pattern("%^[%T] %n: %v%$");
    logSinks[1]->set_pattern("%^[%T] %n: %v%$");

    coreLogger = std::make_unique<spdlog::logger>("VkPBR", logSinks.begin(), logSinks.end());

    clientLogger = std::make_unique<spdlog::logger>("Client", logSinks.begin(), logSinks.end());

#ifdef TRACE
    coreLogger->set_level(spdlog::level::trace);
    coreLogger->flush_on(spdlog::level::trace);
    clientLogger->set_level(spdlog::level::trace);
    clientLogger->flush_on(spdlog::level::trace);
#else
#ifdef DEBUG
    coreLogger->set_level(spdlog::level::info);
    coreLogger->flush_on(spdlog::level::info);
    clientLogger->set_level(spdlog::level::info);
    clientLogger->flush_on(spdlog::level::info);
#else
    coreLogger->set_level(spdlog::level::warn);
    coreLogger->flush_on(spdlog::level::warn);
    clientLogger->set_level(spdlog::level::warn);
    clientLogger->flush_on(spdlog::level::warn);
#endif
#endif

    return true;
}

VKAPI_ATTR VkBool32 VKAPI_CALL Log::debugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData, void* userData) {

    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        coreWarn(callbackData->pMessage);
    } else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
        coreError(callbackData->pMessage);
    }

    return VK_FALSE;
}