project "VkPBR"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    
    targetdir ("../../bin/%{cfg.buildcfg}/%{prj.name}")
    objdir ("../../bin/int/%{cfg.buildcfg}/%{prj.name}")

    pchheader "pch.hpp"
    pchsource "pch.cpp"

    excludes {
        "stdafx.hpp"
    }

    files {
        "**.hpp",
        "**.cpp"
    }

    includedirs {
        "./"
    }
    addIncludes("../")

    links {
        "glfw",
        "imgui",
        "shaderc_shared",
        "spirv-reflect",
        "vk-bootstrap"
    }

    filter "configurations:Debug"
        defines {
            "DEBUG",
            "_DEBUG"
        }
        symbols "On"

    filter "configurations:Release"
        defines "RELEASE"
        optimize "On"

    filter "system:windows"
        libdirs {
            "%{LibDir.vulkan}/"
        }

        links {
            "vulkan-1"
        }

    filter "system:linux"
        links {
            "dl",
            "pthread",
            "vulkan"
        }
