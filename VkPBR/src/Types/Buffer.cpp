#include "pch.hpp"
#include "Buffer.hpp"

#include "Logging.hpp"

VertexBuffer::VertexBuffer(VmaAllocator allocator, VkBuffer buffer, VmaAllocation allocation)
    : allocator(allocator), buffer(buffer), allocation(allocation) {}

VertexBuffer::~VertexBuffer() {
    Log::coreInfo("Destroying Vertex Buffer");

    if (allocator != VK_NULL_HANDLE && buffer != VK_NULL_HANDLE && allocation != VK_NULL_HANDLE) {
        vmaDestroyBuffer(allocator, buffer, allocation);
    }
}
