workspace "VkPBR"
    architecture "x64"

    staticruntime "On"

    configurations {
        "Debug",
        "Release"
    }

    filter "configurations:Debug"
        defines {
            "DEBUG",
            "_DEBUG"
        }
        symbols "On"

    filter "configurations:Release"
        defines "RELEASE"
        optimize "On"

LibDir = {}

if os.host() == "windows" then
    vulkan_path = os.getenv("VULKAN_SDK")
    LibDir["vulkan"] = "%{vulkan_path}/Lib"

    if vulkan_path ~= nil then
        print("Vulkan SDK located at: " .. vulkan_path)
    else
        error("Vulkan SDK is missing! Please install it. https://vulkan.lunarg.com/")
    end
end

IncludeDir = {}
IncludeDir["thirdparty"] = "thirdparty"
IncludeDir["glfw"] = "thirdparty/glfw/include"
IncludeDir["glm"] = "thirdparty/glm"
IncludeDir["imgui"] = "thirdparty/imgui"
IncludeDir["spdlog"] = "thirdparty/spdlog/include"
IncludeDir["vulkan"] = "%{vulkan_path}/include"
IncludeDir["vulkanmemoryallocator"] = "thirdparty/VulkanMemoryAllocator/include"

function addIncludes (relativePath)
    for k,v in pairs(IncludeDir) do
        includedirs { relativePath .. v }
    end
end

include "VkPBR"

include "examples"