#!/bin/bash

echo ----------------------------
echo Getting submodules
git submodule update --init --progress --recursive
echo ----------------------------

echo ----------------------------
echo Generating Projects
thirdparty/premake/premake5 gmake2
echo ----------------------------
