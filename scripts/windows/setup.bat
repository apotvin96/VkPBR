@echo off

echo ----------------------------
echo Getting submodules
call git submodule update --init --progress --recursive
echo ----------------------------

echo ----------------------------
echo Generating Projects
call thirdparty\premake\premake5.exe vs2019
echo ----------------------------
